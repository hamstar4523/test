before_list = ['item1', 'item2', 'item3','item4','test','test2']
after_list = ['example','item2-1', 'item2-2', 'item3','test2']
ignore_list = [
    "item",
    "generateUser",
    "generateDate"
]

#無視してよい設定を現在の定義から確認
for before_list_index in range(len(before_list)):
    for ignore_index in range(len(ignore_list)):
        if ignore_list[ignore_index] in before_list[before_list_index]:
            before_list[before_list_index] = ""

#無視してよい設定を変換後定義から確認
for after_list_index in range(len(after_list)):
    for ignore_index in range(len(ignore_list)):
        if ignore_list[ignore_index] in after_list[after_list_index]:
            after_list[after_list_index] = ""

print("-----現在の定義のみにある設定-----")
print(set(before_list) - set(after_list))
print("-----変換後のみにある設定-----")
print(set(after_list) - set(before_list))
